//
//  SceneDelegate.swift
//  Superheroes
//
//  Created by Abu_zakariya on 20.01.2022.
//

import UIKit
import CoreData

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    var container: NSPersistentContainer!
    
    func createContainer(completion: @escaping (NSPersistentContainer) -> ()) {
        let container = NSPersistentContainer(name: "Model")
        
        container.loadPersistentStores { _, error in
            guard error == nil else {
                fatalError("Failed to load store")
            }
            
            DispatchQueue.main.async {
                completion(container)
            }
        }
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let commandListController = CommandListViewController()
        let commandListNavigationController = UINavigationController(rootViewController: commandListController)
        commandListNavigationController.navigationBar.tintColor = .black
        
        createContainer { container in
            commandListController.dataManager.context = container.viewContext
        }

        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = commandListNavigationController
        window?.makeKeyAndVisible()
    }


}

