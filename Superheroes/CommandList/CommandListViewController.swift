//
//  CommandListViewController.swift
//  Superheroes
//
//  Created by Abu_zakariya on 20.01.2022.
//

import UIKit

final class CommandListViewController: UIViewController {
    
    let dataManager: CommandListDataManager

    private let tableView: UITableView = {
        let table = UITableView()
        table.rowHeight = 80
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        return table
    }()
    
    init() {
        dataManager = CommandListDataManager()

        super.init(nibName: nil, bundle: nil)
        
        dataManager.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let plusImage = UIImage(named: "plus")
        
        navigationItem.title = "Команды"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: plusImage, style: .done, target: self, action: #selector(plusButtonTapped))

        view.backgroundColor = .white
        
        setupTableView()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(CommandListTableViewCell.self, forCellReuseIdentifier: CommandListTableViewCell.reuseId)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    @objc func plusButtonTapped() {
        let controller = ChangeTeamViewController(team: nil, type: .new)
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension CommandListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        TeamStorage.shared.teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommandListTableViewCell.reuseId, for: indexPath) as? CommandListTableViewCell else {
            fatalError("Can not dequeue CommandListTableViewCell")
        }
        let team = TeamStorage.shared.teams[indexPath.item]
        cell.configure(item: team)
        return cell
    }
    
}

extension CommandListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let team = TeamStorage.shared.teams[indexPath.item]
        let controller = TeamInfoViewController(team: team)
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
