//
//  CommandListDataManager.swift
//  Superheroes
//
//  Created by Abu_zakariya on 24.01.2022.
//

import Foundation
import CoreData

final class CommandListDataManager {
    
    weak var delegate: CommandListViewController?
    
    var context: NSManagedObjectContext! {
        didSet {
            fetchTeams()
        }
    }
    
    func fetchTeams() {
        let request = NSFetchRequest<TeamModel>(entityName: "TeamModel")
        
        do {
            TeamStorage.shared.teams = try context.fetch(request).map {
                let team = Team(model: $0)
                team.heroes = fetchHeroes(team: $0)
                return team
            }
        } catch {
            print(error)
        }
        
        delegate?.reloadData()
    }
    
    func fetchHeroes(team: TeamModel) -> [Hero] {
        let predicate = NSPredicate(format: "team = %@", team)
        let request = NSFetchRequest<HeroModel>(entityName: "HeroModel")
        request.predicate = predicate

        do {
            return try team.managedObjectContext!.fetch(request).map { Hero(model: $0) }
        } catch {
            print(error)
        }
        
        return []
    }
    
    func saveTeam(team: Team) {
        guard let context = context else { return }
        
        guard let teamModel = NSEntityDescription.insertNewObject(forEntityName: "TeamModel", into: context) as? TeamModel else { return }
        
        teamModel.teamName = team.name
        
        let heroesSet = NSMutableOrderedSet()
        
        for hero in team.heroes {
            guard let heroModel = NSEntityDescription.insertNewObject(forEntityName: "HeroModel", into: context) as? HeroModel else { return }

            heroModel.heroName = hero.name
            heroModel.heroPhrase = hero.phrase
            heroModel.heroEquipment = hero.equipment
            heroesSet.add(heroModel)
        }
        
        teamModel.teamHeroes = NSOrderedSet(set: heroesSet.set)
        
        do {
            try context.save()
        } catch {
            print(error)
        }
    }
    
}
