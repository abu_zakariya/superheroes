//
//  TeamHeroesSectionDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 23.01.2022.
//

import UIKit

class TeamHeroesSectionDataSource: BaseDataSource {
    
    private let tableView: UITableView
    
    private let team: Team
    
    init(team: Team, tableView: UITableView) {
        self.team = team
        self.tableView = tableView
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        team.heroes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TeamInfoTableViewCell.reuseId, for: indexPath) as? TeamInfoTableViewCell else {
            fatalError()
        }
        let hero = team.heroes[indexPath.item]
        cell.delegate = self
        cell.configure(item: hero, isLeader: indexPath.item == 0, isLeaderChangable: true)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Герои в команде"
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .normal, title: "Удалить") { [weak self] _, _, _ in
            self?.team.heroes.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        }
        action.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        108
    }
    
    override func makeLeader(cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }

        let moveItem = team.heroes.remove(at: indexPath.item)
        team.heroes.insert(moveItem, at: 0)
        
        tableView.reloadData()
    }
    
}
