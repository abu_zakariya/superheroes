//
//  TeamAddHeroSectionDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 23.01.2022.
//

import UIKit

class TeamAddHeroSectionDataSource: BaseDataSource {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddItemTableViewCell.reuseId, for: indexPath) as? AddItemTableViewCell else {
            fatalError()
        }
        cell.configure(item: "Добавить героя")
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        44
    }
    
}
