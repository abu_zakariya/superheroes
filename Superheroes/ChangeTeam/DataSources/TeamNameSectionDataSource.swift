//
//  TeamNameSectionDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 23.01.2022.
//

import UIKit

class TeamNameSectionDataSource: BaseDataSource {
    
    private let team: Team
    
    init(team: Team) {
        self.team = team
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.reuseId, for: indexPath) as? TextFieldTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        cell.configure(item: team.name)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Название команды"
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        44
    }
    
    override func valueChanged(value: String) {
        team.name = value
    }
    
}
