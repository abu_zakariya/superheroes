//
//  ChangeTeamViewController.swift
//  Superheroes
//
//  Created by Abu_zakariya on 21.01.2022.
//

import UIKit

final class ChangeTeamViewController: UIViewController {
    
    enum ScreenType {
        case new, modify
    }

    private let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.separatorStyle = .none
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    private let dataSources: [BaseDataSource]
    
    private let team: Team
    
    private let type: ScreenType
    
    init(team: Team?, type: ScreenType) {
        if team == nil {
            self.team = Team(name: "", heroes: [])
        } else {
            self.team = team!
        }

        self.type = type
        
        dataSources = [
            TeamNameSectionDataSource(team: self.team),
            TeamHeroesSectionDataSource(team: self.team, tableView: self.tableView),
            TeamAddHeroSectionDataSource()
        ]
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveButtonTapped))
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.title = team.name
        
        setupTableView()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(TextFieldTableViewCell.self, forCellReuseIdentifier: TextFieldTableViewCell.reuseId)
        tableView.register(TeamInfoTableViewCell.self, forCellReuseIdentifier: TeamInfoTableViewCell.reuseId)
        tableView.register(AddItemTableViewCell.self, forCellReuseIdentifier: AddItemTableViewCell.reuseId)
    }
    
    @objc func saveButtonTapped() {
        if team.heroes.count < 3 {
            return showFailAlert()
        }
        
        guard let rootViewContoller = navigationController?.viewControllers.first as? CommandListViewController else {
            return
        }
        
        if type == .new {
            TeamStorage.shared.teams.append(team)
            rootViewContoller.dataManager.saveTeam(team: team)
        }

        rootViewContoller.reloadData()
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func showFailAlert() {
        let alert = UIAlertController(title: "Ошибка", message: "Количество героев в команде должно быть не меньше трех", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func addNewHeroToTeam(hero: Hero) {
        team.heroes.append(hero)
        tableView.reloadData()
    }
    
}

extension ChangeTeamViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        dataSources.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSources[section].tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        dataSources[indexPath.section].tableView(tableView, cellForRowAt: indexPath)
    }

}

extension ChangeTeamViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        dataSources[section].tableView(tableView, titleForHeaderInSection: section)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        dataSources[indexPath.section].tableView(tableView, trailingSwipeActionsConfigurationForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        dataSources[indexPath.section].tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let controller = AddHeroViewController()
            controller.delegate = self
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
