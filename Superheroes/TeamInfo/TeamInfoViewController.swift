//
//  TeamInfoViewController.swift
//  Superheroes
//
//  Created by Abu_zakariya on 21.01.2022.
//

import UIKit

final class TeamInfoViewController: UIViewController {
    
    private let team: Team
    
    private let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.rowHeight = 108
        return table
    }()
    
    init(team: Team) {
        self.team = team
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        navigationItem.title = team.name
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Изменить", style: .plain, target: self, action: #selector(changeTeamButtonTapped))
        
        setupTableView()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(TeamInfoTableViewCell.self, forCellReuseIdentifier: TeamInfoTableViewCell.reuseId)
    }
    
    @objc func changeTeamButtonTapped() {
        let controller = ChangeTeamViewController(team: team, type: .modify)
        navigationController?.pushViewController(controller, animated: true)
    }

}

extension TeamInfoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        team.heroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TeamInfoTableViewCell.reuseId, for: indexPath) as? TeamInfoTableViewCell else {
            fatalError("Can not dequeue TeamInfoTableViewCell")
        }
        let hero = team.heroes[indexPath.item]
        cell.configure(item: hero, isLeader: indexPath.item == 0, isLeaderChangable: false)
        return cell
    }
    
}

extension TeamInfoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Герои в команде"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        36
    }
    
}
