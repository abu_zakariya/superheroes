//
//  TeamInfoTableViewCell.swift
//  Superheroes
//
//  Created by Abu_zakariya on 21.01.2022.
//

import UIKit

final class TeamInfoTableViewCell: UITableViewCell {
    
    static let reuseId = "TeamInfoTableViewCell"
    
    weak var delegate: BaseDataSource?
    
    private let heroNameLabelLeadingOffset: CGFloat = 16
    private let heroNameLabelTopOffset: CGFloat = 16
    
    private let phraseLabelTopOffset: CGFloat = 8
    
    private let equipmentLabelTopOffset: CGFloat = 8

    private let heroNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let phraseLabel: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    private let equipmentLabel: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let makeLeaderButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(UIImage(named: "crown"), for: .normal)
        return button
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        makeLeaderButton.addTarget(self, action: #selector(makeLeaderButtonTapped), for: .touchDown)
        
        setupContent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupContent() {
        contentView.addSubview(heroNameLabel)
        contentView.addSubview(phraseLabel)
        contentView.addSubview(equipmentLabel)
        contentView.addSubview(makeLeaderButton)
        
        heroNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: heroNameLabelLeadingOffset).isActive = true
        heroNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: heroNameLabelTopOffset).isActive = true
        
        phraseLabel.leadingAnchor.constraint(equalTo: heroNameLabel.leadingAnchor).isActive = true
        phraseLabel.topAnchor.constraint(equalTo: heroNameLabel.bottomAnchor, constant: phraseLabelTopOffset).isActive = true
        
        equipmentLabel.leadingAnchor.constraint(equalTo: phraseLabel.leadingAnchor).isActive = true
        equipmentLabel.topAnchor.constraint(equalTo: phraseLabel.bottomAnchor, constant: equipmentLabelTopOffset).isActive = true
        equipmentLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -heroNameLabelLeadingOffset).isActive = true
        
        makeLeaderButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        makeLeaderButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -heroNameLabelLeadingOffset).isActive = true
    }
    
    func configure(item: Hero, isLeader: Bool, isLeaderChangable: Bool) {
        heroNameLabel.text = item.name
        if isLeader {
            heroNameLabel.text? += " - Лидер"
        }
        phraseLabel.text = "Коронная фраза: \(item.phrase)"
        equipmentLabel.text = "Снаряжение: \(item.equipment)"
        
        if isLeaderChangable && !isLeader {
            makeLeaderButton.isHidden = false
        } else {
            makeLeaderButton.isHidden = true
        }
    }
    
    @objc func makeLeaderButtonTapped() {
        delegate?.makeLeader(cell: self)
    }
}
