//
//  AddHeroViewController.swift
//  Superheroes
//
//  Created by Abu_zakariya on 22.01.2022.
//

import UIKit

class AddHeroViewController: UIViewController {
    
    weak var delegate: ChangeTeamViewController?
    
    private let networkService = AddHeroNetworkService()

    private let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.separatorStyle = .none
        table.rowHeight = 44
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    private let dataSources = [
        NameSectionDataSource(),
        PhraseSectionDataSource(),
        EquipmentDataSource()
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        let saveButton = UIBarButtonItem(title: "Сохранить", style: .done, target: self, action: #selector(saveButtonTapped))
        let generateButton = UIBarButtonItem(image: UIImage(named: "magic"), style: .done, target: self, action: #selector(generateButtonTapped))
        
        navigationItem.rightBarButtonItems = [saveButton, generateButton]
        
        networkService.delegate = self
        
        setupTableView()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(TextFieldTableViewCell.self, forCellReuseIdentifier: TextFieldTableViewCell.reuseId)
    }
    
    @objc func saveButtonTapped() {
        guard let nameDataSource = dataSources[0] as? NameSectionDataSource,
              let phraseDataSource = dataSources[1] as? PhraseSectionDataSource,
              let equipmentDataSource = dataSources[2] as? EquipmentDataSource
        else { return }
        
        let equipmentArray = equipmentDataSource.equipment
        
        let hero = Hero(name: nameDataSource.name, phrase: phraseDataSource.phrase, equipment: equipmentArray)
        
        delegate?.addNewHeroToTeam(hero: hero)
        
        navigationController?.popViewController(animated: true)
    }
    
    @objc func generateButtonTapped() {
        networkService.sendGenerateRequest()
    }
    
    func updateNewHeroContent(strings: [String]) {
        guard let nameDataSource = dataSources[0] as? NameSectionDataSource,
              let phraseDataSource = dataSources[1] as? PhraseSectionDataSource,
              let equipmentDataSource = dataSources[2] as? EquipmentDataSource
        else { return }
        
        nameDataSource.name = strings[0]
        phraseDataSource.phrase = strings[1]
        equipmentDataSource.equipment = strings[2...4].joined(separator: ",")
        
        tableView.reloadData()
    }

}

extension AddHeroViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        dataSources.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSources[section].itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        dataSources[indexPath.section].tableView(tableView, cellForRowAt: indexPath)
    }
    
}

extension AddHeroViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        dataSources[section].tableView(tableView, titleForHeaderInSection: section)
    }
    
}
