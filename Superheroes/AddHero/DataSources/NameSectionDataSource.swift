//
//  NameSectionDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 22.01.2022.
//

import UIKit

class NameSectionDataSource: BaseDataSource {
    
    var name = ""
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.reuseId, for: indexPath) as? TextFieldTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        cell.configure(item: name)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Имя"
    }
    
    override func valueChanged(value: String) {
        name = value
    }
    
}
