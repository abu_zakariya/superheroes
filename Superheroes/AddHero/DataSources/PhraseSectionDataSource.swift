//
//  PhraseSectionDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 22.01.2022.
//

import UIKit

class PhraseSectionDataSource: BaseDataSource {
    
    var phrase = ""
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.reuseId, for: indexPath) as? TextFieldTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        cell.configure(item: phrase)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Коронная фраза"
    }
    
    override func valueChanged(value: String) {
        phrase = value
    }
    
}
