//
//  EquipmentDataSource.swift
//  Superheroes
//
//  Created by Abu_zakariya on 22.01.2022.
//

import UIKit

class EquipmentDataSource: BaseDataSource {
    
    var equipment = ""
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.reuseId, for: indexPath) as? TextFieldTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        cell.configure(item: equipment)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Снаряжение"
    }
    
    override func valueChanged(value: String) {
        equipment = value
    }
    
}
