//
//  Models.swift
//  Superheroes
//
//  Created by Abu_zakariya on 24.01.2022.
//

final class Team {
    
    var name: String
    var heroes: [Hero]
    
    convenience init(model: TeamModel) {
        let name = model.teamName ?? ""
        let heroesArray = model.teamHeroes?.array as? [Hero] ?? []
        
        self.init(name: name, heroes: heroesArray)
    }
    
    init(name: String, heroes: [Hero]) {
        self.name = name
        self.heroes = heroes
    }
    
}

final class Hero {
    
    var name: String
    var phrase: String
    var equipment: String
    
    convenience init(model: HeroModel) {
        let name = model.heroName ?? ""
        let phrase = model.heroPhrase ?? ""
        let equipment = model.heroEquipment ?? ""
        
        self.init(name: name, phrase: phrase, equipment: equipment)
    }
    
    init(name: String, phrase: String, equipment: String) {
        self.name = name
        self.phrase = phrase
        self.equipment = equipment
    }
    
}
